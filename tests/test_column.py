"tests for table types"
import pytest

sqltypes_ddl = """\
CREATE TABLE sqltypes (
    C_01  CHAR(5),
    C_02  VARCHAR(5),
    C_03  INTEGER,
    C_04  BIGINT,
    C_05  SMALLINT,
    C_06  BYTEINT,
    C_07  DECIMAL(6),
    C_08  DECIMAL(38,4),
    C_09  REAL,
    C_10  DOUBLE PRECISION,
    C_11  NUMBER,
    C_12  NUMBER(10),
    C_13  NUMBER(18,2),
    C_14  DATE,
    C_15  TIME,
    C_16  TIMESTAMP(0),
    C_17  TIMESTAMP(6),
    C_18  INTERVAL YEAR(3),
    C_19  INTERVAL MONTH(3),
    C_20  INTERVAL DAY(4),
    C_21  INTERVAL HOUR(4),
    C_22  INTERVAL MINUTE(4),
    C_23  INTERVAL SECOND,
    C_24  INTERVAL YEAR TO MONTH,
    C_25  INTERVAL DAY TO HOUR,
    C_26  INTERVAL DAY TO MINUTE,
    C_27  INTERVAL DAY TO SECOND,
    C_28  INTERVAL HOUR TO MINUTE,
    C_29  INTERVAL HOUR(4) TO SECOND(2),
    C_30  INTERVAL MINUTE TO SECOND,
    C_31  JSON,
    C_32  JSON,
    C_33  XML
)"""

sqltypes_ins = """\
insert into sqltypes
values (
    cast('abc'  as char(5)),
    cast('abc'  as varchar(5)),
    cast(123456 as integer),
    cast(1234567890123 as bigint),
    cast(1234   as smallint),
    cast(123    as byteint),
    cast(123 as decimal(6)),
    cast(123.56 as decimal(38,4)),
    cast(123e56 as real),
    cast(123e56 as DOUBLE PRECISION),
    cast(123.56 as NUMBER),
    cast(123456 as NUMBER(10)),
    cast(123.56 as NUMBER(18,2)),
    current_date,
    current_time,
    Current_timestamp(0),
    Current_timestamp(6),
    INTERVAL -'100' YEAR,
    INTERVAL '106' MONTH,
    INTERVAL '107' DAY,
    INTERVAL '108' HOUR,
    INTERVAL '109' MINUTE,
    INTERVAL '10' SECOND,
    INTERVAL '05-10' YEAR TO MONTH,
    INTERVAL '06 11' DAY TO HOUR,
    INTERVAL -'07 00:01' DAY TO MINUTE,
    INTERVAL '08 13:20:55' DAY TO SECOND,
    INTERVAL '11:63' HOUR TO MINUTE,
    INTERVAL '11:20:66' HOUR TO SECOND,
    INTERVAL '20:58' MINUTE TO SECOND,
    new JSON('{"ABC":[1,2]}'),
    new JSON('[1,2]'),
    cast('<a/>' as xml)
)"""

@pytest.fixture(scope="module")
def cols(dbcsr):
	from tdtypes import Table
	tbl = Table('tdtypes_test', 'sqltypes', 'T', get_xmldef=dbcsr.get_xmldef)
	try:
		dbcsr.execute(f'DROP TABLE {tbl}')
	except:
		pass
	dbcsr.execute(sqltypes_ddl)
	dbcsr.execute(sqltypes_ins)

	return tbl.columns

def test_colcount(cols):
	assert len(cols) == 33

def test_colnames(cols):
	assert [c.name for c in cols] == [f'C_{n:02d}' for n in range(1, len(cols)+1)]

def test_coltypes(cols):
	assert [c.sqltype() for c in cols] == [
			"CHAR(5)",
			"VARCHAR(5)",
			"INTEGER",
			"BIGINT",
			"SMALLINT",
			"BYTEINT",
			"DECIMAL(6,0)",
			"DECIMAL(38,4)",
			"FLOAT",
			"FLOAT",
			"NUMBER",
			"NUMBER(10,0)",
			"NUMBER(18,2)",
			"DATE",
			"TIME(6)",
			"TIMESTAMP(0)",
			"TIMESTAMP(6)",
			"INTERVAL YEAR(3)",
			"INTERVAL MONTH(3)",
			"INTERVAL DAY(4)",
			"INTERVAL HOUR(4)",
			"INTERVAL MINUTE(4)",
			"INTERVAL SECOND",
			"INTERVAL YEAR TO MONTH",
			"INTERVAL DAY TO HOUR",
			"INTERVAL DAY TO MINUTE",
			"INTERVAL DAY TO SECOND",
			"INTERVAL HOUR TO MINUTE",
			"INTERVAL HOUR(4) TO SECOND(2)",
			"INTERVAL MINUTE TO SECOND",
			"JSON",
			"JSON",
			"XML"
		]
