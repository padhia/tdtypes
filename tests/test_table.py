"tests for table types"
import pytest
from tdtypes import Table, View

def test_pi_cols(dbcsr):
	assert [c.name for c in Table('dbc', 'TVM', 'T', dbcsr.get_xmldef).pi_cols] == ['DatabaseId', 'TVMNameI']

def test_view_cols(dbcsr):
	assert [c.name for c in View('DBC', 'DBCInfo', dbcsr.get_xmldef)] == ['InfoKey', 'InfoData']

def test_view_refs(dbcsr):
	assert [str(r) for r in View('DBC', 'DBCInfo', dbcsr.get_xmldef).refs] == ["DBC.DBCInfoTbl"]
