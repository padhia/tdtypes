import pytest

@pytest.fixture(scope="module")
def ddlcmpr(dbcsr):
	from tdtypes import Table

	def norm_ddl(tbl, ddl):
		dbcsr.execute(ddl)
		dbcsr.execute(f'SHOW TABLE {tbl}')
		return ''.join('\n'.join(r[0].splitlines()) for r in dbcsr.fetchall()).rstrip().rstrip(';')

	def chkdef(tbname: Table, ddl: str) -> bool:
		tbl = Table(dbcsr.schema, tbname, get_xmldef=dbcsr.get_xmldef)
		try:
			dbcsr.execute(f'DROP TABLE {tbl}')
		except:
			pass

		ddl_orig = norm_ddl(tbl, ddl)
		tbdef = tbl.sqldef()
		dbcsr.execute(f'DROP TABLE {tbl}')
		ddl_new = norm_ddl(tbl, tbdef)

		assert ddl_orig == ddl_new

	return chkdef

def test_pi(ddlcmpr):
	ddlcmpr("test_pi", "create multiset table test_pi (k1 integer not null, c1 varchar(100), c2 varchar(100)) primary index(c1,c2)")

def test_nusi(ddlcmpr):
	ddlcmpr("test_nusi", "create multiset table test_nusi (k1 integer not null, c1 varchar(100), c2 varchar(100)) primary index(k1) index (c1,c2)")

def test_nusi_name(ddlcmpr):
	ddlcmpr("test_nusi_name", "create multiset table test_nusi_name (k1 integer not null, c1 varchar(100), c2 varchar(100)) primary index(k1) index nusi(c1,c2)")

def test_vosi(ddlcmpr):
	ddlcmpr("test_vosi", "create multiset table test_vosi (k1 integer not null, c1 varchar(100), c2 varchar(100)) primary index(k1) index vosi(k1,c2) order by(k1)")

def test_nopi(ddlcmpr):
	ddlcmpr("test_nopi", "create multiset table test_nopi (k1 integer not null, c1 varchar(100), c2 varchar(100)) no primary index")

def test_default(ddlcmpr):
	ddlcmpr("test_default", "create multiset table test_default (c1 varchar(128) default user, c2 time(0) default current_time(0), c3 timestamp with default)")

def test_sysver(ddlcmpr):
	ddlcmpr("MIGRATION", """\
CREATE MULTISET TABLE MIGRATION (
    DPLY_TAG   VARCHAR(256)  NOT NULL,
    GIT_BR     VARCHAR(256)  NOT NULL,
    PRD_VER    SMALLINT      NOT NULL DEFAULT 0,
    QA_VER     SMALLINT      NOT NULL DEFAULT 0,
    TEST_VER   SMALLINT      NOT NULL DEFAULT 0,
    ROW_OPENED_TS TIMESTAMP(6) WITH TIME ZONE NOT NULL GENERATED ALWAYS AS ROW START,
    ROW_CLOSED_TS TIMESTAMP(6) WITH TIME ZONE NOT NULL GENERATED ALWAYS AS ROW END,
    PERIOD FOR SYSTEM_TIME (ROW_OPENED_TS, ROW_CLOSED_TS)
)
PRIMARY INDEX(DPLY_TAG)
PARTITION BY CASE_N(END(SYSTEM_TIME) >= CURRENT_TIMESTAMP, NO CASE)
WITH SYSTEM VERSIONING""")
