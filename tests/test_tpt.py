"tests for pattern finder"
from textwrap import dedent

import pytest

from tdtypes import Table
from tdtypes.tpt import *

class DBTbl(Table):
	@property
	def src(self):
		return f"lock row access select * from src.{self.name}"

class FileTbl(Table):
	@property
	def src(self):
		return f"{self.name}.txt"

tgt_auth = TPTVars.from_auth('user', 'secret', 'dbc', prefix='Target')
src_auth = TPTVars.from_auth('user2', 'secret2', 'dbc2', prefix='Source')

def test_vars():
	v = TPTVars()
	v['ATTR1'] = 'VAL1'
	v['ATTR2'] = ['VAL1', 'VAL2']

	assert v.as_attr() == " ATTR(ATTR1='VAL1',ATTR2=['VAL1', 'VAL2'])"
	assert v.as_decl() == dedent("""\
		SET ATTR1 = 'VAL1';
		SET ATTR2 = ['VAL1', 'VAL2'];""")

	v['ATTR1'] = None
	assert v.as_attr() == " ATTR(ATTR2=['VAL1', 'VAL2'])"

	v['ATTR2'] = None
	assert v.as_attr() == ""

def test_ddlstep():
	s = DDLStep('DDLTEST', [DBTbl('tgt',f'tb{t+1}') for t in range(2)], "DELETE FROM {}".format)

	assert str(s) == dedent("""\
		STEP DDLTEST (
			APPLY 'DELETE FROM tgt.tb1',
				'DELETE FROM tgt.tb2' TO OPERATOR ($DDL);
		);""")

def test_odbc():
	op = OdbcOp(DBTbl('tgt','tb1'), conn='DSN=TEST;UID=USER;PWD=SECRET')
	assert str(op) == dedent("""\
		$ODBC ATTR(SelectStmt='lock row access select * from src.tb1',
			ConnectString='DSN=TEST;UID=USER;PWD=SECRET',
			TruncateData='Yes')""")

def test_s3():
	from tdtypes.tpt.util import file_attrs

	v = TPTVars()
	v.update(file_attrs('S3://bucket/pfx', 'file.ext'))

	assert list(v.keys()) == ['AccessModuleName', 'AccessModuleInitStr']
	assert v['AccessModuleName'] == 'libs3axsmod.so'
	assert str(v['AccessModuleInitStr']) == 'S3Bucket=bucket S3Object=pfx/file.ext'

def test_tempdb():
	assert str(LoadOp(Table('db', 'tb'), temp_db='util_work')) == dedent("""\
		$LOAD ATTR(TargetTable='db.tb',
			LogTable='util_work.tb_RL',
			ErrorTable1='util_work.tb_ET',
			ErrorTable2='util_work.tb_UV')""")

	assert str(UpdateOp(Table('db', 'tb'), temp_db='util_work')) == dedent("""\
		$UPDATE ATTR(TargetTable='db.tb',
			LogTable='util_work.tb_RL',
			ErrorTable1='util_work.tb_ET',
			ErrorTable2='util_work.tb_UV',
			WorkTable='util_work.tb_WT')""")

def test_file2db():
	tb1, tb2, tb3 = (FileTbl('tgt', f'tb{t+1}') for t in range(3))

	job = TPTJob('FILE2DB')

	job.varlist.append(tgt_auth)

	job.add_step(LoadStep(tb1, FileReaderOp(tb1), LoadOp(tb1)))
	job.add_step(LoadStep(tb2, FileReaderOp(tb2), UpdateOp(tb2)))
	job.add_step(LoadStep(tb3, FileReaderOp(tb3), InserterOp(10)))
	job.refactor_attrs()

	assert str(job) == dedent("""\
		DEFINE JOB FILE2DB
		(
			SET TargetTdpId        = 'dbc';
			SET TargetUserName     = 'user';
			SET TargetUserPassword = 'secret';

			SET LoadTargetTable         = 'tgt.tb1';
			SET FileReaderFormat        = 'Binary';
			SET FileReaderIndicatorMode = 'Yes';
			SET UpdateTargetTable       = 'tgt.tb2';
			SET InserterMaxSessions     = 10;

			STEP tb1 (
				APPLY
				$INSERT 'tgt.tb1' TO OPERATOR (
					$LOAD
				)
				SELECT * FROM OPERATOR (
					$FILE_READER('tgt.tb1') ATTR(FileName='tb1.txt')
				);
			);

			STEP tb2 (
				APPLY
				$INSERT 'tgt.tb2' TO OPERATOR (
					$UPDATE
				)
				SELECT * FROM OPERATOR (
					$FILE_READER('tgt.tb2') ATTR(FileName='tb2.txt')
				);
			);

			STEP tb3 (
				APPLY
				$INSERT 'tgt.tb3' TO OPERATOR (
					$INSERTER
				)
				SELECT * FROM OPERATOR (
					$FILE_READER('tgt.tb3') ATTR(FileName='tb3.txt')
				);
			);
		);""").replace('\t', '    ')

def test_delimited():
	tb1, tb2, tb3 = (FileTbl('tgt', f'tb{t+1}') for t in range(3))

	job = TPTJob('DELIMITED')

	job.varlist.append(tgt_auth)

	job.steps.append(LoadStep(tb1, FileReaderOp(tb1, dlm='|'), LoadOp(tb1)))
	job.steps.append(LoadStep(tb2, FileReaderOp(tb2, dlm='|'), UpdateOp(tb2)))
	job.steps.append(LoadStep(tb3, FileReaderOp(tb3, dlm='|'), InserterOp(10)))

	job.refactor_attrs()

	assert str(job) == dedent("""\
		DEFINE JOB DELIMITED
		(
			SET TargetTdpId        = 'dbc';
			SET TargetUserName     = 'user';
			SET TargetUserPassword = 'secret';

			SET LoadTargetTable         = 'tgt.tb1';
			SET FileReaderFormat        = 'Delimited';
			SET FileReaderTextDelimiter = '|';
			SET UpdateTargetTable       = 'tgt.tb2';
			SET InserterMaxSessions     = 10;

			STEP tb1 (
				APPLY
				$INSERT 'tgt.tb1' TO OPERATOR (
					$LOAD
				)
				SELECT * FROM OPERATOR (
					$FILE_READER(DELIMITED 'tgt.tb1') ATTR(FileName='tb1.txt')
				);
			);

			STEP tb2 (
				APPLY
				$INSERT 'tgt.tb2' TO OPERATOR (
					$UPDATE
				)
				SELECT * FROM OPERATOR (
					$FILE_READER(DELIMITED 'tgt.tb2') ATTR(FileName='tb2.txt')
				);
			);

			STEP tb3 (
				APPLY
				$INSERT 'tgt.tb3' TO OPERATOR (
					$INSERTER
				)
				SELECT * FROM OPERATOR (
					$FILE_READER(DELIMITED 'tgt.tb3') ATTR(FileName='tb3.txt')
				);
			);
		);""").replace('\t', '    ')

def test_db2db():
	tb1, tb2 = (DBTbl('tgt',f'tb{t+1}') for t in range(2))
	qb = [QB1('JOBNAME', 'DB2DB'), QB1('APPLICATION', 'PYTEST')]

	job = TPTJob('DB2DB')

	job.add_step(DDLStep('TruncAll', [tb1, tb2], "DELETE FROM {}".format))
	job.add_step(LoadStep(tb1, ExportOp(tb1, util_sz=UtilSize.LARGE, qb=qb), LoadOp(tb1, qb=qb)))
	job.add_step(LoadStep(tb2, SelectorOp(tb2, qb=qb), UpdateOp(tb2, util_sz=UtilSize.SMALL, qb=qb)))

	assert str(job) == dedent("""\
		DEFINE JOB DB2DB
		(
			STEP TruncAll (
				APPLY 'DELETE FROM tgt.tb1',
					'DELETE FROM tgt.tb2' TO OPERATOR ($DDL);
			);

			STEP tb1 (
				APPLY
				$INSERT 'tgt.tb1' TO OPERATOR (
					$LOAD ATTR(TargetTable='tgt.tb1',
						QueryBandSessInfo=['JOBNAME=DB2DB;', 'APPLICATION=PYTEST;'])
				)
				SELECT * FROM OPERATOR (
					$EXPORT ATTR(SelectStmt='lock row access select * from src.tb1',
						QueryBandSessInfo=['JOBNAME=DB2DB;', 'APPLICATION=PYTEST;', 'UtilityDataSize=LARGE;'])
				);
			);

			STEP tb2 (
				APPLY
				$INSERT 'tgt.tb2' TO OPERATOR (
					$UPDATE ATTR(TargetTable='tgt.tb2',
						QueryBandSessInfo=['JOBNAME=DB2DB;', 'APPLICATION=PYTEST;', 'UtilityDataSize=SMALL;'])
				)
				SELECT * FROM OPERATOR (
					$SELECTOR ATTR(SelectStmt='lock row access select * from src.tb2',
						QueryBandSessInfo=['JOBNAME=DB2DB;', 'APPLICATION=PYTEST;'])
				);
			);
		);""").replace('\t', '    ')

def test_auth():
	tb1 = DBTbl('tgt','tb1')

	job = TPTJob('TESTAUTH')
	job.varlist.append(tgt_auth)
	job.varlist.append(src_auth)
	job.steps.append(LoadStep(tb1, ExportOp(tb1), LoadOp(tb1)))
	job.refactor_attrs()

	assert str(job) == dedent("""\
		DEFINE JOB TESTAUTH
		(
			SET TargetTdpId        = 'dbc';
			SET TargetUserName     = 'user';
			SET TargetUserPassword = 'secret';

			SET SourceTdpId        = 'dbc2';
			SET SourceUserName     = 'user2';
			SET SourceUserPassword = 'secret2';

			SET LoadTargetTable  = 'tgt.tb1';
			SET ExportSelectStmt = 'lock row access select * from src.tb1';

			STEP tb1 (
				APPLY
				$INSERT 'tgt.tb1' TO OPERATOR (
					$LOAD
				)
				SELECT * FROM OPERATOR (
					$EXPORT
				);
			);
		);""").replace('\t', '    ')
