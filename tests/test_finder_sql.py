"test pattern finder SQL generation"
from textwrap import dedent
import pytest
from tdtypes import DBObjPat, DBObjFinder

def test_patterns():
	assert str(DBObjPat("%")) == "DatabaseName = DATABASE"
	assert str(DBObjPat("%.%")) == "DatabaseName LIKE '%'"
	assert str(DBObjPat("tb%")) == "DatabaseName = DATABASE AND TableName LIKE 'tb%'"
	assert str(DBObjPat("%.tb%")) == "TableName LIKE 'tb%'"
	assert str(DBObjPat("db%.%")) == "DatabaseName LIKE 'db%'"
	assert str(DBObjPat("tmp_db?.%")) == "DatabaseName LIKE 'tmp+_db_' ESCAPE '+'"
	assert str(DBObjPat("db%.%tb%")) == "DatabaseName LIKE 'db%' AND TableName LIKE '%tb%'"
	assert str(DBObjPat("!%tmp%.%tmp%")) == "(DatabaseName NOT LIKE '%tmp%' OR TableName NOT LIKE '%tmp%')"
	assert str(DBObjPat("!%.%tmp%")) == "TableName NOT LIKE '%tmp%'"

def test_finder():
	i1 = DBObjPat("db%.%tb2%")
	i2 = DBObjPat("%.tb%")
	x1 = DBObjPat("!%tmp%.%")
	x2 = DBObjPat("!%.%tmp%")

	assert DBObjFinder([i1, i2, x1]).sql_pred() == dedent("""\
		CASE
			WHEN DatabaseName LIKE '%tmp%' THEN NULL
			WHEN DatabaseName LIKE 'db%' AND TableName LIKE '%tb2%' THEN 0
			WHEN TableName LIKE 'tb%' THEN 1
			ELSE NULL
		END IS NOT NULL""")

	assert DBObjFinder([x1, x2], db='DBName', tb='TBName').sql_pred() == dedent("""\
		CASE
			WHEN DBName LIKE '%tmp%' THEN NULL
			WHEN TBName LIKE '%tmp%' THEN NULL
			ELSE -1
		END IS NOT NULL""")

	# special case for pattern lists containing only 1 pattern
	assert DBObjFinder([i1]).sql_pred() == str(i1)
	assert DBObjFinder([x1]).sql_pred() == str(x1)
