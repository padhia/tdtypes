"tests for pattern finder"
from textwrap import dedent

from tdtypes.util import Preds

p1 = Preds(["x", None, "y", ""])
p2 = Preds([None, ""])
p3 = Preds(["a", None, "b", ""], any=True)
p4 = Preds(["a", None, "b", ""], any=True, paren=True)
p5 = Preds(["a", None, ""], any=True, paren=True)

def test_iter():
	assert list(p1) == ["x", "y"]
	assert list(p2) == []
	assert list(p3) == ["a", "b"]

def test_str():
	assert str(p1) == "x AND y"
	assert str(p2) == ""

def test_any():
	assert str(Preds(["x", None, ""], any=True)) == "x"
	assert str(p3) == "a OR b"
	assert str(p4) == "(a OR b)"
	assert str(p5) == "a"

def test_nested():
	assert str(Preds([p1, "z", p2, p3])) == "x AND y AND z AND a OR b"
	assert str(Preds([p1, "z", p2, p4])) == "x AND y AND z AND (a OR b)"

def test_where():
	assert p1.where() == "WHERE x AND y"
	assert p2.where() == ""
