"global setup including fixtures"
import pytest

@pytest.fixture(scope="session")
def dbcsr():
	from tdtypes.dbapi import cursor
	with cursor(None) as csr:
		try:
			csr.execute("Create database tdtypes_test From sysdba As Perm=10e6")
		except:
			pass
		csr.execute('database tdtypes_test')
		yield csr
