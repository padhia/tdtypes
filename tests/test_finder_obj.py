"test pattern finder results"
import pytest
from tdtypes import DBObjPat, DBObjFinder

@pytest.fixture(scope="module")
def finder(dbcsr):
	return lambda *args, **kwargs: DBObjPat.findall([DBObjPat(p) for p in args], **kwargs, csr=dbcsr)

def test_1pat_to_1obj(finder):
	tables = finder('Sys_Calendar.CALENDAR')
	assert len(tables) == 1
	assert str(tables[0]).lower() == 'sys_calendar.calendar'

def test_1pat_to_0obj(finder):
	tables = finder('tdtypes_test.non_existing_table', warn_notfound=False)
	assert len(tables) == 0

def test_1pat_to_many(finder):
	from tdtypes import Table, View
	tables = finder('dbc.dbcinfo%')
	assert len(tables) == 3
	assert sum(1 for t in tables if isinstance(t, Table)) == 1
	assert sum(1 for t in tables if isinstance(t, View)) == 2

def test_with_types(finder):
	assert len(finder('dbc.dbcinfo%', objtypes='TO')) == 1
	assert len(finder('dbc.dbcinfo%', objtypes='V')) == 2
