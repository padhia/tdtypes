#! /usr/bin/env python
"package setup script"

import glob
from setuptools import setup

with open("README.md", encoding="utf-8") as f:
	readme = f.read()

setup(
	name='tdtypes',
	description='Database Types for Teradata',
	long_description=readme,
	long_description_content_type="text/markdown",
	url='https://bitbucket.org/padhia/tdtypes',

	author='Paresh Adhia',
	version='0.5.1',
	license='MIT',
	python_requires='>=3.7',

	packages=['tdtypes', 'tdtypes.tpt'],
	data_files=[('tdtypes/samples', glob.glob('samples/*.py')), ('tdtypes', ['LICENSE', 'README.md'])],
	install_requires=['cached-property', 'teradatasql'],
	keywords='teradata',

	classifiers=[
		'Development Status :: 4 - Beta',
		'Intended Audience :: Developers',
		'License :: OSI Approved :: MIT License',
		'Topic :: Database',
		'Topic :: Software Development',

		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.7',
		'Programming Language :: Python :: 3.8',
	],
)
