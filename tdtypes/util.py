"logging module for the tdtypes package"

__author__ = "Paresh Adhia"
__copyright__ = "Copyright 2016-2020, Paresh Adhia"

from typing import Any, Iterator, Optional
import re
import os
import logging

_log_level = None


class Ident(str):
	"Database Identifier"
	# pylint: disable=locally-disabled, bad-whitespace, multiple-statements
	def __eq__(self, other: Optional[Any]) -> bool: return isinstance(other, str) and self.upper() == other.upper()
	def __lt__(self, other: Optional[Any]) -> bool: return isinstance(other, str) and self.upper() < other.upper()
	def __le__(self, other: Optional[Any]) -> bool: return self.__eq__(other) or self.__lt__(other)
	def __ge__(self, other: Optional[Any]) -> bool: return not self.__lt__(other)
	def __ne__(self, other: Optional[Any]) -> bool: return not self.__eq__(other)
	def __gt__(self, other: Optional[Any]) -> bool: return not self.__le__(other)
	def __contains__(self, other: Optional[Any]) -> bool: return isinstance(other, str) and str.__contains__(self.upper(), other.upper())

	def __hash__(self) -> int:
		return self.upper().__hash__()

	def __format__(self, spec: str = '') -> str:
		"formatting for quoted identifiers"
		if spec and 'q' in spec:
			spec = spec.replace('q', 's')
			if not re.fullmatch('[a-z#$_][a-z0-9#$_]*', str.__str__(self), re.I):
				return str.__format__('"' + str.__str__(self).replace('"', '""') + '"', spec)

		return str.__format__(self, spec)


class Preds(list):
	"Class that represents list of predicates"
	def __init__(self, *pred: Any, any: bool = False, paren: bool = False) -> None:
		super().__init__(*pred)
		self.any = any
		self.paren = paren

	def where(self) -> str:
		"returns a WHERE clause if there are non-empty predicates, else an empty string"
		s = str(self)
		return f"WHERE {s}" if s else ""

	def __iter__(self) -> Iterator[str]:
		"returns an iterator of non-empty string values of predicates"
		return (str(i) for i in super().__iter__() if i and str(i))

	def __bool__(self) -> bool:
		"returns true if at least one item is true"
		return any(self)

	def __str__(self) -> str:
		"returns an SQL expression of all non-empty predicates"
		s = (" OR " if self.any else " AND ").join(self)
		if self.paren and self.any and len(list(self)) > 1:
			s = "(" + s + ")"

		return s


def getLogger(name: Optional[str]) -> logging.Logger:
	"get logger object for the module name"
	global _log_level

	logger = logging.getLogger(name)
	if _log_level is None:
		_log_level = os.environ.get('TDLOGLEVEL', "NOTSET")
		logging.basicConfig(format="%(levelname)s: %(message)s")

	try:
		logger.setLevel(_log_level)
	except ValueError:
		logging.getLogger(__name__).error('"{}" is an invalid value for $TDLOGLEVEL, ignored.'.format(_log_level))
		_log_level = "NOTSET"

	return logger


def indent(s: Any, num: int = 1) -> str:
	"indent individual lines of string by adding specified number of tabs"
	pfx = '\t' * num
	return '\n'.join(pfx + l if l else l for l in str(s).splitlines())  # don't indent empty lines


def indent2(s: Any, num: int = 1) -> str:
	"indent individual lines, starting with 2nd line, of string by adding specified number of tabs"
	pfx = '\t' * num
	return '\n'.join(pfx + l if e and l else l for e, l in enumerate(str(s).splitlines()))  # don't indent 1st line or empty lines


def strip_pfx(s: str, pfx: Optional[str] = None) -> str:
	"""
	strip prefix from each line if there is one. If prefix is None (default) the it is set to
	string up to the first non-space character of the first line
	"""
	sl = s.splitlines()
	if not sl:
		return s

	if pfx is None:
		pos = next((p[0] for p in enumerate(sl[0]) if not p[1].isspace()), None)
		if pos is None:
			return s
		pos += 1
		_pfx = sl[0][:pos]
	else:
		_pfx = pfx
		pos = len(_pfx)

	return '\n'.join(map(lambda s: s[pos:] if s.startswith(_pfx) else s, sl))
