"Module containing functions to obtain Teradata cursor"

__author__ = "Paresh Adhia"
__copyright__ = "Copyright 2016-2019 Paresh Adhia"

from typing import Optional, Any
from .cursor import Connection, Cursor

try:
	from tdconn_site import *
except ImportError:
	from .tdconn_default import *

conn: Optional[Connection] = None  # The first (and usually the only) connection object
csr: Optional[Cursor] = None  # The latest (and usually the only) cursor object


def connect(*args: Any, **kargs: Any) -> Connection:
	"return a datbase connection object"
	return Connection(dbconnect(*args, **kargs))


def cursor(*args: Any, **kargs: Any) -> Cursor:
	"return a new cursor object using the global connection object, and also save it as the global cursor object"
	global conn, csr

	if conn is None:
		import atexit
		conn = connect(*args, **kargs)
		atexit.register(conn.close)

	csr = conn.cursor()

	return csr


def commit() -> None:
	"commit using the global connection object"
	if conn is not None:
		conn.commit()
