"Teradata Table and View types"

__author__ = "Paresh Adhia"
__copyright__ = "Copyright 2016-2020, Paresh Adhia"

from warnings import warn
warn("tdtypes.table is deprecated, use tdtypes", FutureWarning)

from .tvm import DBObj, Table, NoPITable, TemporaryTable, VolatileTable, GTTable, View
from .finder import DBObjPat, DBObjFinder
