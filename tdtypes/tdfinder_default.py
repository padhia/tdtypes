"Teradata Find framework to help object instantiation"
from __future__ import annotations

__author__ = "Paresh Adhia"
__copyright__ = "Copyright 2016-2020, Paresh Adhia"

from typing import Optional, List, Union, Iterable, Tuple
from textwrap import dedent

from .tvm import DBObj, XMLDefGetter
from .dbapi import Cursor
from .util import getLogger, Preds, indent2, strip_pfx

logger = getLogger(__name__)


class DBObjPat:
	"[!][<DB-PAT>.]<TB-PAT>; supported wild-card characters: [%%,*,?]; e.g. DB1.TB%%, DB?.TB1*, !db2.tb2%%, tb2?last"
	def __init__(self, pat: str):
		self.pat = pat
		if pat.startswith('!'):
			self.incl = False
			pat = pat[1:]
		else:
			self.incl = True
		self.sch, self.name = pat.split('.') if '.' in pat else (None, pat)

	def sqlrepr(self, db: str = 'DatabaseName', tb: str = 'TableName', match_ovr: Optional[bool] = None) -> str:
		"build SQL search condition using search predicates formed by schema and name"
		incl = self.incl if match_ovr is None else match_ovr

		cond = Preds(any=not incl, paren=True)
		if self.sch is None and incl:
			cond.append(self.search_predicate(db, None, defval='DATABASE'))
		elif self.sch != '%':
			cond.append(self.search_predicate(db, self.sch, match=incl))
		if self.name != "%":
			cond.append(self.search_predicate(tb, self.name, match=incl))

		if not cond:
			cond.append(self.search_predicate(db, "%", match=incl))

		return str(cond)

	search_cond = sqlrepr  # legacy compatibility name
	__str__ = sqlrepr

	def __repr__(self) -> str:
		return f"'{self.pat}'"

	@staticmethod
	def search_predicate(col: str, val: Optional[str], defval: Optional[str] = None, match: bool = True) -> Optional[str]:
		"build SQL search predicate based on if value contains any wild-card characters"
		eq, like = ('=', 'LIKE') if match else ('<>', 'NOT LIKE')

		if val is None:
			if defval is None:
				return None
			return col + f' {eq} ' + defval

		if '%' in val or '?' in val or '*' in val:
			esc = " ESCAPE '+'" if '_' in val else ''
			val = val.replace('_', '+_').replace('?', '_').replace('*', '%')
			return col + f" {like} '{val}'{esc}"

		return col + f" {eq} '{val}'"

	@staticmethod
	def findall(
		patterns: Iterable[Union[DBObjPat, str]],
		objtypes: str = '',
		flatten: bool = True,
		warn_notfound: bool = True,
		csr: Optional[Cursor] = None
	) -> Union[List[DBObj], List[List[DBObj]]]:
		"factory method that returns list of objects matching list of wild-cards"
		from . import dbapi
		return DBObjFinder(patterns, objtypes).findall(csr if csr is not None else dbapi.cursor(None), flatten=flatten, warn_notfound=warn_notfound)


class DBObjFinder:
	"Class to find all DBObj that match a list of DBOBjPat"
	def __init__(
		self,
		patterns: Iterable[Union[DBObjPat, str]],
		objtypes: str = '',
		db: str = 'DatabaseName',
		tb: str = 'TableName',
		kind: str = 'TableKind'
	):
		self.patterns = [DBObjPat(p) if isinstance(p, str) else p for p in patterns]
		self.objtypes, self.db, self.tb, self.kind = objtypes, db, tb, kind

	@property
	def caseexpr(self) -> str:
		"returns SQL CASE expression that evaluates to the index of the matched pattern"
		from itertools import chain, tee

		patseq = enumerate(self.patterns)  # number each pattern
		i1, i2 = tee(patseq)
		# sequence non-matches before matches
		patseqxi = chain(filter(lambda x: not x[1].incl, i1), filter(lambda x: x[1].incl, i2))

		# default match is any if all are exclude-types, otherwise, default is non-match
		default_match = 'NULL' if any(p for p in self.patterns if p.incl) else '-1'

		def when(pos: int, pat: DBObjPat) -> str:
			return f'WHEN {pat.sqlrepr(self.db, self.tb, match_ovr=True)} THEN ' + (str(pos) if pat.incl else 'NULL')

		return dedent("""\
			CASE
				{}
				ELSE {}
			END""").format('\n\t'.join(when(e, p) for e, p in patseqxi), default_match)

	@property
	def kindexpr(self) -> Optional[str]:
		"return 'TableKind' expression"
		if not self.objtypes:
			return None

		if len(self.objtypes) == 1:
			return f"{self.kind} = '{self.objtypes}'"

		return self.kind + " IN (" + ",".join(f"'{t}'" for t in self.objtypes) + ")"

	def to_preds(self) -> Preds:
		"return SQL predicate that checks for any (non-)matching patterns"
		if len(self.patterns) == 1:
			qual = self.patterns[0].search_cond(db=self.db, tb=self.tb)
		else:
			qual = self.caseexpr + " IS NOT NULL"

		return Preds([qual, self.kindexpr])

	def sql_pred(self, indent: int = 0) -> str:
		"return SQL predicate as string"
		return indent2(str(self.to_preds()), indent)

	def match_col(self, indent: str = "\t") -> str:
		"returns indented SQL CASE expression that evaluates to the index of the matched pattern"
		return self.caseexpr.replace('\n', '\n' + indent)

	def make_sql(self) -> str:
		"Build SQL to find objects matching patterns"
		return strip_pfx(f"""\
			|SELECT DatabaseName
			|	, TableName
			|	, CASE WHEN CommitOpt <> 'N' THEN 't' ELSE TableKind END AS TableKind
			|	, {indent2(self.caseexpr)} AS MATCHED_PAT
			|FROM dbc.TablesV T
			|WHERE {Preds(["MATCHED_PAT IS NOT NULL", self.kindexpr])}""")

	def names2objs(
		self,
		names: List[Tuple[str, str, str, int]],
		flatten: bool = True,
		warn_notfound: bool = True,
		get_xmldef: Optional[XMLDefGetter] = None
	) -> Union[List[DBObj], List[List[DBObj]]]:
		"group names by pattern index that they belong to"
		matches: List[List[DBObj]] = [[] for p in self.patterns]
		for db, tb, tp, m in names:
			matches[m].append(DBObj.create(db, tb, tp, get_xmldef))

		if warn_notfound:
			all_notfound = ', '.join(repr(p) for p, m in zip(self.patterns, matches) if p.incl and not m)
			if all_notfound:
				logger.warning('No matching objects found for: %s', all_notfound)

		return [o for m in matches for o in m] if flatten else matches

	def findall(
		self,
		csr: Cursor,
		flatten: bool = True,
		warn_notfound: bool = True
	) -> Union[List[DBObj], List[List[DBObj]]]:
		"returns list of matching objects"
		sql = self.make_sql() + '\nORDER BY MATCHED_PAT, 1, 2'
		logger.debug('Search patterns: {}, SQL:{}\n'.format(', '.join(str(p) for p in self.patterns), sql))
		csr.execute(sql)

		return self.names2objs([r[:4] for r in csr.fetchall()], flatten=flatten, warn_notfound=warn_notfound, get_xmldef=csr.get_xmldef)
